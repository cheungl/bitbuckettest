# Bitbucket test
## This is something to test if we can push/pull from both bitbucket and GE github.
See also this [blog post](http://blog.kevinlee.io/2013/03/11/git-push-to-pull-from-both-github-and-bitbucket/) for more information about this process.

## Step 1: Create a bitbucket repo.
Go to bitbucket.org

Get/Create an account (if necessary).

Create a repo (this one is called bitbuckettest)

## Step 2: Log into GE machines and initial repo with stuff.

set the https_proxy

```
git clone https://cheungl@bitbucket.org/cheungl/bitbuckettest.git
cd bitbuckettest
echo "# My project's README" >> README.md
git add README.md
git commit -m "Initial commit"
git push -u origin master
```
Enter your bitbucket password when prompted.

Right now it's only going to be accessing bitbucket:
```
[cheung@ares12 bitbuckettest]$ git remote -v show
origin	https://cheungl@bitbucket.org/cheungl/bitbuckettest.git (fetch)
origin	https://cheungl@bitbucket.org/cheungl/bitbuckettest.git (push)
```


## Step 3: Duplicate it on GE github.

Go to https://github.build.ge.com.

Log in, create same repo (again, bitbuckettest in this example).

## Step 4: Add a remote origin 

On the GE machines, add a remote origin to the same git hub directory
```
git remote set-url origin --add git@github.build.ge.com:200019653/bitbuckettest.git
```
Check to make sure that it worked:
```
[cheung@ares12 bitbuckettest]$ git remote -v show
origin	https://cheungl@bitbucket.org/cheungl/bitbuckettest.git (fetch)
origin	https://cheungl@bitbucket.org/cheungl/bitbuckettest.git (push)
origin	git@github.build.ge.com:200019653/bitbuckettest.git (push)
```
## Step 5: Push to both repos
```
git push -u origin master
```
Next time you can do just a `git push` and both repos will be updated.


